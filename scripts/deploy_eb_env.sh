#!/bin/bash -e

REGION=$AWS_DEFAULT_REGION
STACK_NAME=StackSet-$CIRCLE_PROJECT_REPONAME
TEMPLATE=cfn/template.yml
CUSTOMER=$CUSTOMER_NAME
PURPOSE=hosting

function create_env_file() {
export ENV_NAME=$1
if [ -z $ENV_NAME ]; then
  echo "USE: create_env_file ENV_NAME"
  exit 1
else
  echo -e "\x1b[32mCreating the enviroment configuration file for the Elastic Beanstalk application\x1b[0m"
  mkdir -p ./vars/environment/$ENV_NAME
  cat ./vars/environment/prod/vars.yml | sed '3s/New/Existing/g' | sed '4s/prod/'"$ENV_NAME"'/g' > ./vars/environment/$ENV_NAME/vars.yml
  echo -e "\x1b[31mCreated the configuration file into the ./vars/environment/$ENV_NAME/vars.yml:\x1b[0m"
  echo -e "\x1b[32m$(cat ./vars/environment/$ENV_NAME/vars.yml)\x1b[0m\n"
  
fi
}

if ! $(command -v yamllint >/dev/null 2>&1); then
    pip install yamllint
fi

export branch_env=$(echo $CIRCLE_BRANCH | cut -d'-' -f 1)

if [ $branch_env = "mdcpss" ]; then
  export env="${CIRCLE_BRANCH//-}"
  create_env_file $env
elif [ $branch_env = "master" ]; then
  export env="prod"
elif [ $branch_env = "dev" ]; then
  export env="development"
  create_env_file $env
else
  echo -e "\x1b[31mBranch $branch_env not recognized, only enable, dev, master, mdcpss-XYZ\x1b[0m"
  exit 1
fi

set -exo pipefail
cfn-lint --ignore-checks W1020 W2001 W8001 -t $TEMPLATE
yamllint -d "{extends: relaxed, rules: {line-length: {max: 180}}}" $TEMPLATE

echo -e "Start the cloudformation deployment - \x1b[31m REGION: $REGION, ENV: $env, STACK_NAME: $STACK_NAME and TEMPLATE: $TEMPLATE\x1b[0m\n"
$DOCKER_CMD_PREFIX digops-stacks change-set $STACK_NAME $TEMPLATE \
      --environment $env \
      --region $REGION \
      --customer $CUSTOMER \
      --purpose $PURPOSE
$DOCKER_CMD_PREFIX digops-stacks execute $STACK_NAME \
      --environment $env \
      --region $REGION

exit 0
